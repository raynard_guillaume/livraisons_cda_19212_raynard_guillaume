var btnStart = document.getElementById("start");
var btnPause = document.getElementById("pause");
var btnStop = document.getElementById("stop");
var span = document.getElementById('span');
var tpsEcoule = 0;
var decompte = 0;
span.innerHTML = ajouteUnZero(0) +":"+ajouteUnZero(0)+":"+ajouteUnZero(0);
(function(){
    btnStart.paramTps = tpsEcoule; 
    btnPause.paramTps = tpsEcoule;
    btnStart.addEventListener("click",startChrono, false);
})();

function startChrono(e){
    btnStart.removeEventListener("click", startChrono,false);
    btnStart.style.display = "none";
    btnPause.style.display = "block" ;
    btnStop.style.display = "block" ;
    btnPause.removeEventListener("click", startChrono, false);
    btnPause.addEventListener("click", pauseChrono, false);
    btnStop.addEventListener("click", stopChrono, false);
    var startTime = new Date(); 
    decompte = setInterval(function() { 
    // 1- Convertir en secondes : 
    var seconds = Math.round((new Date().getTime() - startTime.getTime()) / 1000 + e.target.paramTps); 
    // e représente l'event déclencheur           
      // e.target représente l'objet déclencheur      
             // ici : bouton start ou bouton pause
             // (cette prop a été ajoutée aux boutons)
             // 2- Extraire les heures: 
             var hours = parseInt( seconds / 3600 ); 
             seconds = seconds % 3600; // secondes restantes 
              // 3- Extraire les minutes: 
              var minutes = parseInt( seconds / 60 );       
              seconds = seconds % 60; // secondes restantes       
              // 4- afficher dans le span       
              span.innerHTML = ajouteUnZero(hours) +":"+ajouteUnZero(minutes)+":"+ajouteUnZero(seconds);
              // 5- incrémenter le nombre de secondes       
              tpsEcoule += 1; }, 1000);
}

function ajouteUnZero(nombre){
    if (parseInt(nombre)<10){
        return 0+""+nombre;
    } else {
        return nombre;
    }
}

function pauseChrono(){
    clearInterval(decompte);
    btnPause.removeEventListener("click", pauseChrono, false);
    btnPause.addEventListener("click", startChrono, false);
    btnPause.paramTps = tpsEcoule;
    console.log(btnPause.paramTps);
}

function stopChrono(){
    clearInterval(decompte);
    btnPause.removeEventListener("click", pauseChrono, false);
    btnPause.removeEventListener("click", startChrono, false);
    btnStop.removeEventListener("click", stopChrono, false);
    btnStart.addEventListener("click",startChrono, false);
    tpsEcoule = 0;
    decompte = 0;
    span.innerHTML = ajouteUnZero(0) +":"+ajouteUnZero(0)+":"+ajouteUnZero(0);
    btnPause.style.display = "none" ;
    btnStop.style.display = "none" ;
    btnStart.style.display = "block";
}

